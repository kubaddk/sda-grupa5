import React from "react";
import PropTypes from "prop-types";
import { PropProductData } from "../helpers/interfaces";
import { ListItem, List, ListItemText, Card } from "@mui/material";
import "./homepage.css";
import DB from "../Database.json";

const HomePage = () => {
  const flowers = Object.values(DB);

  const sold = () => {
    alert("Congrats, you got a new flower!!!");
  };

  return (
    <>
      <h1>Power Flower</h1>
      <div>
        {flowers.length !== 0 &&
          flowers.map((flower) => {
            return (
              <List>
                <Card
                  variant="outlined"
                  sx={{ m: "10px", width: "30%", float: "left", p: "20px" }}
                >
                  <div className="product">
                    <h3 className="title">{flower.name}</h3>

                    <img
                      src={flower.urlPic}
                      width="250"
                      height="200"
                      alt=""
                      className={flower.isSold ? "blackandwhite" : ""}
                    />

                    <p className="price">Cena kwiata: {flower.price}</p>

                    <p className="info">Opis kwiata: {flower.name}</p>

                    <button onClick={sold}>Add to cart</button>
                  </div>
                </Card>
              </List>
            );
          })}
      </div>
    </>
  );
};

export default HomePage;
