import React from "react";
import { Link } from "react-router-dom";
import LoginForm from "../../LoginForm/LoginForm/LoginForm";


const LoginPage = () => {
  return (
    <>
      <LoginForm />
      <h6 style={{ fontWeight: 100, textAlign: "center" }}>
        If you don't have an account yet:
      </h6>
      <Link to="/register" style={{textDecoration: 'none'}}>
      <button type="submit" style={{display:"block", margin:"1rem", outlineStyle:"dotted"}}>Register </button>
      </Link>
    </>
  );
};

export default LoginPage;