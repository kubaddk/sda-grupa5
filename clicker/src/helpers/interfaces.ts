export interface flower {
  uid: string;
  nazwa: string;
  cena: number;
  urlPic: string;
  buyer: string;
  isSold: boolean;
}

export interface LoginFormData {
    email: string;
    password: string;
  }

export interface RegisterFormData {
  email: string;
  password: string;
  password2: string;
}

export interface ProductData {
    uid: string;
    name: string;
    price: number;
    urlPic: string;
    buyer: string;
    isSold: boolean;
}

export interface PropProductData {
  prop: ProductData;
}


