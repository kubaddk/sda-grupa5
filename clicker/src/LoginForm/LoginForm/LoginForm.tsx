import React from 'react'
import { useForm } from "react-hook-form";
import { signInWithEmailAndPassword } from "firebase/auth";
import { LoginFormData } from '../../helpers/interfaces';
import { auth } from "../../firebaseConfig";


const LoginForm = () => {
    const { register, handleSubmit } = useForm<LoginFormData>();
    const logUserIn = ({ email, password }:LoginFormData) => {
        signInWithEmailAndPassword(auth, email, password)
          .then(() => {
            console.log("Logged in");
          })
          .catch((err) => console.error(err.message));
      };

  return (

<form
      style={{ display: "flex", flexDirection: "column" }}
      onSubmit={handleSubmit(logUserIn)}
    >
      <input
        type="email"
        placeholder="email"
        style={{ display: "block", margin:"0.5rem"}}
        {...register("email", { required: true })}
      />
      <input
        type="password"
        placeholder="password"
        style={{ display: "block", margin:"0.5rem"}}
        {...register("password", { required: true })}
      />
      <button type="submit" style={{display:"block", margin:"1rem", outlineStyle:"dotted"}}>Log in</button>
    </form>
  )
}

export default LoginForm