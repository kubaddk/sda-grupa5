// Import the functions you need from the SDKs you need
import { initializeApp } from "firebase/app";
import { getAuth } from "firebase/auth";
import { getStorage } from "firebase/storage";

// Your web app's Firebase configuration
const firebaseConfig = {
  apiKey: "AIzaSyA26uF2okCan48NBqiebEpj-R65ak2a34A",
  authDomain: "sda-flowers.firebaseapp.com",
  projectId: "sda-flowers",
  storageBucket: "sda-flowers.appspot.com",
  messagingSenderId: "1080974658899",
  appId: "1:1080974658899:web:9b21803280706888e4a5a6",
};

// Initialize Firebase
const app = initializeApp(firebaseConfig);
export const auth = getAuth(app);
export const storage = getStorage(app);
