import React from "react";
import Navbar from "./Navbar";
import LoginPage from "./LoginPage/LoginPage/LoginPage";
import RegisterPage from "./components/RegisterPage/RegisterPage";
import HomePage from "./HomePage/HomePage";
import DB from "./Database.json"
import { PropProductData } from "./helpers/interfaces";
import NewFlowerPage from "./NewFlowerPage/NewFlowerPage";
// import FlowersBasketPage from "./";
import { BrowserRouter, Routes, Route } from "react-router-dom";



function App() {
  return (
    <div className="App">
      <BrowserRouter>
        <Navbar />
        <Routes>
          <Route path="/" element={<HomePage/>} />
          <Route path="/login" element={<LoginPage />} />
          <Route path="/register" element={<RegisterPage />} />
          <Route path="/new-flower" element={<NewFlowerPage />} />
          {/* <Route path="/flowers-basket" element={<FlowersBasketPage />} /> */}
        </Routes>
      </BrowserRouter>
    </div>
  );
}

export default App;
