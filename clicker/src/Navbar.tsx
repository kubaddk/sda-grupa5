import React from "react";
import NavbarCSS from "./Navbar.module.css";
const Navbar = () => {
  return (
    <nav className={NavbarCSS.nav}>
      <div className={NavbarCSS.btn}><a href="/">Home</a></div>
      <div className={NavbarCSS.btn}><a href="/new-flower">Moje kwiatki</a></div>
      <div className={NavbarCSS.btn}>Koszyk</div>
      <div className={NavbarCSS.btn}><a href="/login">Logowanie</a></div>
      <div className={NavbarCSS.btnLast}><a href="/register">Zaloguj</a></div>
    </nav>
  );
};
export default Navbar;
